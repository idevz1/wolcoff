import React from "react";
import {
  FiFacebook,
  FiTwitter,
  FiInstagram,
  FiLinkedin,
  FiChevronRight,
  FiMapPin,
  FiPhone,
} from "react-icons/fi";
import { Link } from "react-router-dom";

const HeaderTopBar = () => {
  return (
    <div className="header-top-bar">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-4 col-md-12 col-12">
            <div className="header-left">
              {/* <p><a href="#link">Get the most advanced template <FiChevronRight /></a></p> */}
            </div>
          </div>
          <div className="col-lg-8 col-md-12 col-12">
            <div className="header-right">
              <div className="address-content">
                <p>
                  <FiMapPin style={{ color: "#7c4d93" }} />
                  <span style={{ color: "#7c4d93" }}>Curitiba, PR</span>
                </p>
                <p>
                  <FiPhone style={{ color: "#7c4d93" }} />
                  <span style={{ color: "#7c4d93" }}>
                    <a style={{ color: "#7c4d93" }} href="#">
                      +55 41 3206.4241
                    </a>
                  </span>
                </p>
              </div>
              <div className="social-icon-wrapper">
                <ul className="social-icon social-default icon-naked">
                  <li>
                    <a target="_blank" href="https://www.facebook.com/odontologiawolcoff">
                      <FiFacebook style={{ color: "#7c4d93" }} />
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://www.instagram.com/odontologiawolcoff">
                      <FiInstagram style={{ color: "#7c4d93" }} />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderTopBar;
