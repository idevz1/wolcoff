import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import { FiArrowRight } from "react-icons/fi";

const callToActionData = {
  title: "Junte-se à nossa comunidade de pacientes satisfeitos",
  subtitle: "Finest choice for your home & office",
  btnText: "Agende sua consulta conosco!",
};

const CalltoActionFive = () => {
  return (
    <div className="rn-callto-action clltoaction-style-default style-5">
      <div className="container">
        <div className="row row--0 align-items-center content-wrapper theme-shape">
          <div className="col-lg-12">
            <div className="inner">
              <div className="content text-center">
                <ScrollAnimation
                  animateIn="fadeInUp"
                  animateOut="fadeInOut"
                  animateOnce={true}
                >
                  <h2 className="title">{callToActionData.title}</h2>
                </ScrollAnimation>
                {/* <ScrollAnimation
                  animateIn="fadeInUp"
                  animateOut="fadeInOut"
                  animateOnce={true}
                >
                  <h6 className="subtitle">{callToActionData.subtitle}</h6>
                </ScrollAnimation> */}

                <ScrollAnimation
                  animateIn="fadeInUp"
                  animateOut="fadeInOut"
                  animateOnce={true}
                >
                  <div className="call-to-btn text-center">
                    <a
                      className="btn-default btn-icon"
                      href="https://api.whatsapp.com/send?phone=5541997679426&text=Tenho%20interesse%20em%20agendar%20com%20voc%C3%AAs!"
                    >
                      {callToActionData.btnText}{" "}
                      <i className="icon">
                        <FiArrowRight />
                      </i>
                    </a>
                  </div>
                </ScrollAnimation>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CalltoActionFive;
