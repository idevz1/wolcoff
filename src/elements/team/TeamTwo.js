import React from "react";
import { FiFacebook, FiTwitter, FiInstagram, FiLinkedin } from "react-icons/fi";
import { Link } from "react-router-dom";
import ScrollAnimation from "react-animate-on-scroll";

const teamData = [
  {
    image: "team-01",
    name: "Hugo Torres",
    designation: "Cliente",
    location: "CO Miego, AD, USA",
    description:
      "Ótima equipe, excelente atendimento, ambiente agradável e limpo, tudo perfeito. A doutora inspira muita confiança em tudo, me senti muito seguro durante o procedimento. Voltarei sempre que estiver em Curitiba. Obrigado por tudo.",
    socialNetwork: [
      {
        icon: <FiFacebook />,
        url: "#",
      },
      {
        icon: <FiTwitter />,
        url: "#",
      },
      {
        icon: <FiInstagram />,
        url: "#",
      },
    ],
  },
  {
    image: "team-02",
    name: "Silvana Silva",
    designation: "Cliente",
    location: "Bangladesh",
    description:
      "Ótimo atendimento, profissionais super eficientes. A nova estrutura é super agradável e aconchegante, recomendo.",
    socialNetwork: [
      {
        icon: <FiFacebook />,
        url: "#",
      },
      {
        icon: <FiTwitter />,
        url: "#",
      },
      {
        icon: <FiLinkedin />,
        url: "#",
      },
    ],
  },
  {
    image: "team-03",
    name: "Fabio Santos",
    designation: "Cliente",
    location: "Poland",
    description:
      "Atendimento de primeiríssima qualidade, melhor experiência que já tive em dentista. Dra Bruna é muito profissional, super recomendo!!!",
    socialNetwork: [
      {
        icon: <FiFacebook />,
        url: "#",
      },
      {
        icon: <FiTwitter />,
        url: "#",
      },
      {
        icon: <FiInstagram />,
        url: "#",
      },
    ],
  },
];

const TeamTwo = ({ column, teamStyle }) => {
  return (
    <div className="row row--15">
      {teamData.map((data, index) => (
        <div className={`${column}`} key={index}>
          <ScrollAnimation
            animateIn="fadeInUp"
            animateOut="fadeInOut"
            animateOnce={true}
          >
            <div
              id={`${index === 1 ? 'cardone' : index === 2 ? 'cardfinal' : 'cardzero'}`}
              className={`rn-team ${teamStyle}`}
              style={{ background: "rgb(124, 77, 147)" }}
            >
              <div
                className="inner"
                style={{ background: "rgb(124, 77, 147)" }}
              >
                {/* <figure className="thumbnail">
                  <img
                    src={`./images/team/${data.image}.jpg`}
                    alt="Wolcoff"
                  />
                </figure> */}
                <figcaption className="content">
                  <h2 className="title">{data.name}</h2>
                  <h6 className="subtitle theme-gradient">
                    {data.designation}
                  </h6>
                  {/* <span className="team-form">
                    <img
                      src="./images/team/location.svg"
                      alt="Wolcoff"
                    />
                    <span className="location">{data.location}</span>
                  </span> */}
                  <p className="description" style={{ color: "white" }}>
                    {data.description}
                  </p>

                  {/* <ul className="social-icon social-default icon-naked mt--20">
                    {data.socialNetwork.map((social, index) => (
                      <li key={index}>
                        <a href={`${social.url}`}>{social.icon}</a>
                      </li>
                    ))}
                  </ul> */}
                </figcaption>
              </div>
            </div>
          </ScrollAnimation>
        </div>
      ))}
    </div>
  );
};

export default TeamTwo;
